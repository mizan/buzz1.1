package com.mizan;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.jfree.util.Log;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.web.action.admin.user.UserProperty;
import com.atlassian.plugin.PluginParseException;
import com.opensymphony.module.propertyset.PropertyException;
import com.opensymphony.module.propertyset.PropertySchema;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetSchema;
import com.atlassian.crowd.embedded.api.User;

public class PhoneNumber extends AbstractJiraContextProvider{


/* --- for 4.4.x---
	@Override
	public Map getContextMap(com.opensymphony.user.User user, JiraHelper jiraHelper) throws PropertyException {
		// TODO Auto-generated method stub
		Map contextMap = new HashMap();
        Issue currentIssue = (Issue) jiraHelper.getContextParams().get("issue");
		User assignee=(User) currentIssue.getAssigneeUser();
		User reporter=(User) currentIssue.getReporterUser();
		
		contextMap.put("assignee",assignee);
		contextMap.put("reporter",reporter);
		String assigneeValue=ComponentManager.getInstance().getUserPropertyManager().getPropertySet(assignee).getString("jira.meta.phone number");
		String reporterValue=ComponentManager.getInstance().getUserPropertyManager().getPropertySet(reporter).getString("jira.meta.phone number");
		
		if(assignee!=null){
			contextMap.put("assigneeNumber",assigneeValue );
        }
		if(reporter!=null){
			contextMap.put("reporterNumber",reporterValue );
        }
		return contextMap;
	}
*/

	@Override
	public Map getContextMap(com.atlassian.crowd.embedded.api.User user, JiraHelper jiraHelper) throws PropertyException {
		// TODO Auto-generated method stub
		Map contextMap = new HashMap();
        Issue currentIssue = (Issue) jiraHelper.getContextParams().get("issue");
		User assignee=(User) currentIssue.getAssigneeUser();
		User reporter=(User) currentIssue.getReporterUser();
		
		contextMap.put("assignee",assignee);
		contextMap.put("reporter",reporter);
		String assigneeValue=ComponentManager.getInstance().getUserPropertyManager().getPropertySet(assignee).getString("jira.meta.phone number");
		String reporterValue=ComponentManager.getInstance().getUserPropertyManager().getPropertySet(reporter).getString("jira.meta.phone number");
		
		if(assignee!=null){
			contextMap.put("assigneeNumber",assigneeValue );
        }
		if(reporter!=null){
			contextMap.put("reporterNumber",reporterValue );
        }
		return contextMap;
	}
	
}
